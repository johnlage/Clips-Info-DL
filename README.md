Overview
========

A tool to help organize and sort twitch clip metadata (with date/braodcaster filtering settings) into easily usable CSV/JSON files.
Said CSV files can be easily imported into Google Sheets/Microsoft Excel. More filtering/other options coming soon.
Any Bugs, Issues, Suggestions, and Ideas can be left on the issue tracker [here](https://gitlab.com/johnlage/Clips-Info-DL/issues).

-   Links: [Source](https://gitlab.com/johnlage/Clips-Info-DL), [Issues](https://gitlab.com/johnlage/Clips-Info-DL/issues)
-   Free software: Apache Software License 2.0
