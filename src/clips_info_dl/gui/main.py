import wx
import wx.adv
import io
from clips_info_dl.core.main import ClipsInfoDL
from clips_info_dl import __version__
from clips_info_dl.core.errors import UserNotFoundException
from requests.exceptions import ConnectionError as ReqConnectionError
import requests
import pytz
from tzlocal import get_localzone
import datetime
import pathlib
import webbrowser


class BroadcasterInfo(wx.StaticBoxSizer):
    def __init__(self, app, parent):
        super().__init__(wx.VERTICAL, parent, "Broadcaster Info")
        self.app = app

        self.flex_sizer = wx.FlexGridSizer(3, 2, gap=wx.Size(5, 5))
        self.Add(self.flex_sizer, wx.SizerFlags(1).Expand().Border(wx.LEFT | wx.RIGHT | wx.BOTTOM, 5))
        self.flex_sizer.AddGrowableCol(0)

        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Broadcaster Name:"), wx.SizerFlags().Left())
        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Broadcaster Image:"), wx.SizerFlags().Left())

        self.name_text_ctrl = wx.TextCtrl(self.GetStaticBox(), size=wx.Size(160, -1))
        self.flex_sizer.Add(self.name_text_ctrl, wx.SizerFlags().Left())

        self.image = wx.StaticBitmap(self.GetStaticBox(), size=wx.Size(160, 160))
        self.flex_sizer.Add(self.image, wx.SizerFlags().Left())

        self.flex_sizer.Add((0, 0))

        self.lookup_button = wx.Button(self.GetStaticBox(), label="Check Broadcaster")
        self.lookup_button.Bind(wx.EVT_BUTTON, self.do_check)
        self.lookup_button.SetDefault()
        self.flex_sizer.Add(self.lookup_button, wx.SizerFlags().Left())

    def do_check(self, event):
        name = self.name_text_ctrl.GetLineText(0)
        if not name:
            self.app.do_error_popup("Broadcaster name is a required field to continue.", "Missing Information")
        else:
            try:
                self.app.user_info = self.app.core.get_user(name)
                image = wx.Image(io.BytesIO(requests.get(self.app.user_info['profile_image_url']).content))
                image = image.Scale(160, 160)
                self.image.SetBitmap(wx.Bitmap(image))
                self.app.user_check_done()
            except ReqConnectionError:
                self.app.user_check_done(success=False)
                self.app.do_error_popup("Connection Failed. Check your Internet Connection and try again.",
                                        "Connection Failed.")
            except UserNotFoundException:
                self.app.user_check_done(success=False)
                self.app.do_error_popup("Invalid Broadcaster Name.", "Broadcaster Not Found.")


class DateTimeInfo(wx.StaticBoxSizer):
    def __init__(self, app, parent):
        super().__init__(wx.VERTICAL, parent, "Date/Time Info")
        self.app = app

        self.flex_sizer = wx.FlexGridSizer(5, 2, gap=wx.Size(5, 5))
        self.Add(self.flex_sizer, wx.SizerFlags(1).Expand().Border(wx.LEFT | wx.RIGHT | wx.BOTTOM, 5))
        self.flex_sizer.AddGrowableCol(0)

        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Start Date:"), wx.SizerFlags().Left())
        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="End Date:"), wx.SizerFlags().Left())

        self.start_date_ctrl = wx.adv.DatePickerCtrl(self.GetStaticBox(), size=wx.Size(160, -1),
                                                     dt=wx.DateTime.Today().SetDay(1))
        self.flex_sizer.Add(self.start_date_ctrl, wx.SizerFlags().Left())

        self.end_date_ctrl = wx.adv.DatePickerCtrl(self.GetStaticBox(), size=wx.Size(160, -1),
                                                   dt=wx.DateTime.Today().SetToLastMonthDay())
        self.flex_sizer.Add(self.end_date_ctrl, wx.SizerFlags().Left())

        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Start Time:"), wx.SizerFlags().Left())
        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="End Time:"), wx.SizerFlags().Left())

        self.start_time_ctrl = wx.adv.TimePickerCtrl(self.GetStaticBox(), size=wx.Size(160, -1),
                                                     dt=wx.DateTime().FromHMS(0))
        self.flex_sizer.Add(self.start_time_ctrl, wx.SizerFlags().Left())

        self.end_time_ctrl = wx.adv.TimePickerCtrl(self.GetStaticBox(), size=wx.Size(160, -1),
                                                   dt=wx.DateTime().FromHMS(23, 59, 59))
        self.flex_sizer.Add(self.end_time_ctrl, wx.SizerFlags().Left())

        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Timezone:"),
                            wx.SizerFlags().Align(wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT))
        self.all_timezones = pytz.common_timezones
        self.timezone_ctrl = wx.Choice(self.GetStaticBox(), choices=self.all_timezones, size=wx.Size(160, -1))
        self.timezone_ctrl.SetSelection(self.all_timezones.index(get_localzone().zone))
        self.flex_sizer.Add(self.timezone_ctrl, wx.SizerFlags().Left())

    def process_datetimes(self):
        timezone = pytz.timezone(self.timezone_ctrl.GetString(self.timezone_ctrl.GetSelection()))

        start_date = wx.wxdate2pydate(self.start_date_ctrl.GetValue())
        start_time = wx.wxdate2pydate(self.start_time_ctrl.GetValue())
        self.app.start_datetime = timezone.localize(datetime.datetime.combine(start_date.date(), start_time.time()))

        end_date = wx.wxdate2pydate(self.end_date_ctrl.GetValue())
        end_time = wx.wxdate2pydate(self.end_time_ctrl.GetValue())
        self.app.end_datetime = timezone.localize(datetime.datetime.combine(end_date.date(), end_time.time()))


class OutputFileInfo(wx.StaticBoxSizer):
    def __init__(self, app, parent):
        super().__init__(wx.VERTICAL, parent, "Output File Info")
        self.app = app

        self.flex_sizer = wx.FlexGridSizer(5, 2, gap=wx.Size(5, 5))
        self.Add(self.flex_sizer, wx.SizerFlags(1).Expand().Border(wx.LEFT | wx.RIGHT | wx.BOTTOM, 5))
        self.flex_sizer.AddGrowableCol(1)

        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Output File Type:"),
                            wx.SizerFlags().Align(wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT))
        self.type_ctrl = wx.Choice(self.GetStaticBox(), choices=['CSV', 'JSON'])
        self.type_ctrl.SetSelection(0)
        self.flex_sizer.Add(self.type_ctrl, wx.SizerFlags().Left())

        self.flex_sizer.Add(wx.StaticText(self.GetStaticBox(), label="Output Filename:"),
                            wx.SizerFlags().Align(wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT))
        self.name_text = wx.StaticText(self.GetStaticBox(), label='Browse to set a filename...',
                                       style=wx.ST_ELLIPSIZE_START)
        self.flex_sizer.Add(self.name_text,
                            wx.SizerFlags().Align(wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT).Expand())

        self.flex_sizer.Add((0, 0))
        self.name_ctrl = wx.FilePickerCtrl(self.GetStaticBox(), style=wx.FLP_SAVE | wx.FLP_OVERWRITE_PROMPT,
                                           wildcard="Output File Types (*.csv,*.json)|*.csv;*.json")
        self.name_ctrl.Bind(wx.EVT_FILEPICKER_CHANGED, self.on_name_select)
        self.flex_sizer.Add(self.name_ctrl, wx.SizerFlags().Left())

    def on_name_select(self, event):
        self.name_text.SetLabel(self.name_ctrl.GetPath())
        self.app.file_name = pathlib.Path(self.name_ctrl.GetPath())

    def on_type_change(self, event):
        self.app.file_type = self.type_ctrl.GetString(self.type_ctrl.GetSelection())


class MainWindowFrame(wx.Frame):
    def __init__(self, app):
        super().__init__(None, title=f"Clips Info Downloader v{__version__}")
        self.app = app
        self.progress = None

        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        self.main_sizer.AddSpacer(5)

        self.caster_info = BroadcasterInfo(self.app, self)
        self.main_sizer.Add(self.caster_info, wx.SizerFlags().Expand().Border(wx.LEFT | wx.RIGHT, 5))
        self.main_sizer.AddSpacer(5)

        self.datetime_info = DateTimeInfo(self.app, self)
        self.main_sizer.Add(self.datetime_info, wx.SizerFlags().Expand().Border(wx.LEFT | wx.RIGHT, 5))
        self.main_sizer.AddSpacer(5)

        self.file_info = OutputFileInfo(self.app, self)
        self.main_sizer.Add(self.file_info, wx.SizerFlags().Expand().Border(wx.LEFT | wx.RIGHT, 5))
        self.main_sizer.AddSpacer(5)

        self.dl_button = wx.Button(self, label="Download Clips Info")
        self.dl_button.Bind(wx.EVT_BUTTON, self.do_download)
        self.main_sizer.Add(self.dl_button, wx.SizerFlags().Right().Border(wx.RIGHT, 10))
        self.main_sizer.AddSpacer(5)

        self.SetSizerAndFit(self.main_sizer)
        self.Center()

    def do_download(self, event):
        try:
            self._do_download()
        except Exception as e:
            if self.progress:
                self.progress.Destroy()
                self.progress = None
            self.app.do_error_popup(f"Error Type: {e.__class__.__name__}\nFull Error Message: {str(e)}",
                                    "An Unexpected Error Occurred.")

    def _do_download(self):
        self.datetime_info.process_datetimes()
        if not self.verify_fields():
            return

        self.progress = progress = wx.ProgressDialog("Downloading Clips Info...",
                                                     "Downloading Clips info from Twitch...")
        progress.Show()
        progress.Pulse(newmsg="Downloading Clips info from Twitch...")
        clips = self.app.core.get_clips(self.app.user_info['id'], start_at=self.app.start_datetime,
                                        end_at=self.app.end_datetime)

        progress.Pulse(newmsg=f"Found {len(clips)} Clips! Processing...")
        clips = self.app.core.process_clips(clips)

        progress.Pulse(newmsg="Processing Complete. Writing Clips to file...")
        if self.app.file_type == "CSV":
            self.app.core.dump_clips_csv(clips, self.app.file_name)
        elif self.app.file_type == "JSON":
            self.app.core.dump_clips_json(clips, self.app.file_name)
        progress.Update(100, newmsg="Done.")
        progress.Destroy()
        self.progress = None
        done = wx.MessageDialog(self, f"Saved Info for {len(clips)} Clips Successfully.",
                                "Done", style=wx.OK | wx.ICON_INFORMATION)
        done.ShowModal()
        self.reset_file_name()

    def verify_fields(self):
        if not self.app.user_info['login'] == self.caster_info.name_text_ctrl.GetLineText(0):
            self.app.do_error_popup("Broadcaster login typed doesn't match checked broadcaster login.",
                                    "Invalid Information")
            return False

        if not self.app.end_datetime > self.app.start_datetime:
            if self.app.end_datetime == self.app.start_datetime:
                self.app.do_error_popup("Start and End Dates and Times are the same.", "Invalid Information")
            else:
                self.app.do_error_popup("Start Date and Time is after End Date and Time.", "Invalid Information")
            return False

        if not self.app.file_name:
            self.app.do_error_popup("You must select an output file.", "Invalid Information")
            return False
        return True

    def reset_file_name(self):
        self.file_info.name_text.SetLabel('Browse to set a filename...')
        self.file_info.name_ctrl.SetPath('')
        self.app.file_name = None


class ClipsInfoDLGUIApp(wx.App):
    def __init__(self):
        super().__init__()
        self.core = ClipsInfoDL()
        self.user_info = {}
        self.start_time = None
        self.end_time = None
        self.file_name = None
        self.file_type = "CSV"
        self.main_window = MainWindowFrame(self)
        self.user_check_done(success=False)
        self.main_window.Show()
        try:
            self.check_updates()
        except Exception:
            pass

    def do_error_popup(self, message, caption):
        popup = wx.MessageDialog(self.main_window, message=message, caption=caption, style=wx.OK | wx.ICON_ERROR)
        popup.ShowModal()

    def user_check_done(self, success=True):
        for section in [self.main_window.datetime_info, self.main_window.file_info]:
            for child in section.GetStaticBox().GetChildren():
                if hasattr(child, 'Enable'):
                    child.Enable(enable=success)
        self.main_window.dl_button.Enable(enable=success)
        if success:
            self.main_window.dl_button.SetDefault()

    def check_updates(self):
        version = self.core.check_for_updates()
        if version:
            message = f"""A New Version is available:
            New Version: {version['version']}
            Current Version: {self.core.version}
            Would you like to view it now?
            """.replace("   ", "")
            popup = wx.MessageDialog(self.main_window, message=message, caption="A new version is available",
                                     style=wx.YES_NO | wx.CANCEL | wx.ICON_INFORMATION)
            code = popup.ShowModal()
            if code == wx.ID_YES:
                webbrowser.open(version['website_url'], new=2)


def main():
    app = ClipsInfoDLGUIApp()
    app.MainLoop()
