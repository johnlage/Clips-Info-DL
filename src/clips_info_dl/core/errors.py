class ClipsInfoDLException(Exception):
    pass


class UserNotFoundException(ClipsInfoDLException):
    pass


class ClipsNotFoundException(ClipsInfoDLException):
    pass


class UnknownTwitchException(ClipsInfoDLException):
    pass
