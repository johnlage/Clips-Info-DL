import requests
import datetime
import json
import csv
import time
from typing import List, Dict
from collections import OrderedDict
from clips_info_dl.core.errors import UnknownTwitchException, UserNotFoundException, ClipsNotFoundException
from clips_info_dl import __version__
from pkg_resources import parse_version


COLUMNS = {
    'id': 'Clip ID',
    'url': 'Clip URL',
    'embed_url': 'Clip Embed URL',
    'broadcaster_id': 'Broadcaster ID',
    'broadcaster_name': 'Broadcaster Name',
    'creator_id': 'Creator ID',
    'creator_name': 'Creator Name',
    'video_id': 'Video ID',
    'game_id': 'Game ID',
    'game_name': 'Game Name',
    'language': 'Language',
    'title': 'Title',
    'view_count': 'View Count',
    'created_at': 'Created At',
    'thumbnail_url': 'Thumbnail URL'
}
GAME_LOOKUP_COLUMNS = {
    'game_name': 'name'
}


class ClipsInfoDL:
    version = __version__

    def __init__(self):
        self.client_id = "259lz3fua6cmzrq6ok0shrslehja4x"
        self.user_token = None

    def _get_headers(self) -> Dict[str, str]:
        headers = {"Client-ID": self.client_id}
        if self.user_token:
            headers["Authorization"] = f"Bearer {self.user_token}"
        return headers

    def _twitch_api_call(self, endpoint: str, params: dict=None) -> requests.Response:
        loop = True
        r = None
        while loop:
            r = requests.get(url=f"https://api.twitch.tv/helix/{endpoint}", params=params, headers=self._get_headers())
            if r.status_code == 429:
                time.sleep(5)
            else:
                loop = False
        return r

    def get_user(self, channel_name: str) -> dict:
        user_response = self._twitch_api_call('users', {'login': channel_name})
        if not user_response.ok:
            if user_response.status_code == 404:
                raise UserNotFoundException()
            else:
                raise UnknownTwitchException()
        user_response_dict = user_response.json()
        if len(user_response_dict['data']) == 0:
            raise UserNotFoundException()
        return user_response_dict['data'][0]

    def get_clips(self, user_id: int, count: int=None, start_at: datetime.datetime=None,
                  end_at: datetime.datetime=None) -> List[dict]:
        loop = True
        cursor = None
        clips = []
        params = {"broadcaster_id": user_id, 'first': 100}
        request_count = 0

        if start_at:
            params['started_at'] = start_at.isoformat('T')
        if end_at:
            params['ended_at'] = end_at.isoformat('T')

        while loop:
            if cursor:
                params.update({"after": cursor})
            response = self._twitch_api_call('clips', params=params)
            if response.ok:
                r_json = response.json()
                if 'data' in r_json:
                    clips.extend(r_json['data'])
                    request_count += 1
                    if 'cursor' in r_json['pagination'] and r_json['pagination']['cursor']:
                        cursor = r_json['pagination']['cursor']
                    else:
                        loop = False
                else:
                    loop = False
            elif response.status_code == 404:
                if request_count > 0:
                    loop = False
                else:
                    raise ClipsNotFoundException()
            else:
                raise UnknownTwitchException()

        return clips

    def get_games(self, game_ids: list) -> Dict[str, dict]:
        game_id_list_chunks = [game_ids[x:x + 100] for x in range(0, len(game_ids), 100)]
        game_dict = {}
        for chunk in game_id_list_chunks:
            chunk_games = self._twitch_api_call('games', params={'id': chunk}).json()['data']
            for game in chunk_games:
                game_dict[game['id']] = game
        return game_dict

    def process_clips(self, clips: List[dict], columns: List[str] = None) -> List[OrderedDict]:
        if not columns:
            columns = ['url', 'title', 'broadcaster_name', 'creator_name', 'game_name', 'view_count', 'created_at']
        games = []

        for column in columns:
            if column in GAME_LOOKUP_COLUMNS:
                game_ids = []
                for clip in clips:
                    if clip['game_id'] not in game_ids:
                        game_ids.append(clip['game_id'])
                games = self.get_games(game_ids)
                break

        clips_out = []
        for clip in clips:
            clip_out = OrderedDict()
            for column in columns:
                if column in GAME_LOOKUP_COLUMNS:
                    if clip['game_id']:
                        clip_out[column] = games[clip['game_id']][GAME_LOOKUP_COLUMNS[column]]
                    else:
                        clip_out[column] = ''
                else:
                    clip_out[column] = clip[column]
            clips_out.append(clip_out)
        return clips_out

    def dump_clips_json(self, clips: List[dict], file_name: str):
        with open(file_name, 'w') as out:
            json.dump(clips, out)

    def dump_clips_csv(self, clips: List[dict], file_name: str, user_friendly_names: bool=True):
        with open(file_name, 'w', newline='', encoding='utf-8') as out:
            csv_file = csv.writer(out)
            names = clips[0].keys()
            if user_friendly_names:
                names = []
                for column in clips[0].keys():
                    names.append(COLUMNS[column])
            csv_file.writerow(names)
            for clip in clips:
                csv_file.writerow(clip.values())

    def check_for_updates(self):
        versions = requests.get('https://johnlage.com/projects/clips-info-dl/versions.json')
        versions = versions.json()
        if parse_version(self.version) < parse_version(versions['versions'][0]['version']):
            return versions['versions'][0]
        else:
            return False

