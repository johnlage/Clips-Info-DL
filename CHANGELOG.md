Changelog
=========

0.1 (2018-12-03)
------------------

-   Initial Codebase.
-   Includes date and broadcaster filtering.
-   Basic GUI
