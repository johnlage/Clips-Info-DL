import click
import datetime
import pytz
from clips_info_dl.core.main import ClipsInfoDL
from clips_info_dl import __version__


@click.command(name='clips-info-dl')
@click.argument('channel_name', type=click.STRING)
@click.argument('start_date', type=click.DateTime(formats=['%Y-%m-%d']))
@click.argument('end_date', type=click.DateTime(formats=['%Y-%m-%d']))
@click.option('--timezone', '-t', type=click.STRING, default=pytz.UTC)
@click.option('--csv/--json', default=True)
@click.version_option(__version__, prog_name='clips-info-dl')
def main(channel_name: str, start_date: datetime.datetime, end_date: datetime.datetime,
         timezone: str, csv: bool):
    core = ClipsInfoDL()
    version = core.check_for_updates()
    if version:
        click.echo(f"A New Version is available: {version['version']}")
    user = core.get_user(channel_name)
    if timezone:
        timezone = pytz.timezone(timezone)
    start_date = start_date.replace(hour=0, minute=0, second=0, tzinfo=timezone)
    end_date = end_date.replace(hour=23, minute=59, second=59, tzinfo=timezone)
    clips = core.get_clips(user['id'], start_at=start_date, end_at=end_date)
    click.echo(f"Found {len(clips)} Clips! Processing...")
    clips = core.process_clips(clips)
    if csv:
        core.dump_clips_csv(clips, f"{channel_name} {start_date.date().isoformat()} - "
                                   f"{end_date.date().isoformat()} Clips.csv")
    else:
        core.dump_clips_json(clips, f"{channel_name} {start_date.date().isoformat()} - "
                                    f"{end_date.date().isoformat()} Clips.json")
